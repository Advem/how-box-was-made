#include <iostream>
#include <cstdlib>
#include <string>
#include <errno.h>
#include <ctime>
#include <cmath>
#include <vector>
#include <numeric>
#include <sstream>
#include <thread>
#include <chrono>
#include <mutex>
#include <windows.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>

// git test

#pragma warning(disable : 4996)

using namespace std;

const int SCREEN_W = 1000;
const int SCREEN_H = 800;
const float FPS = 30;
bool redraw = true;
bool done = false;
int font_size = 16;
int frame_count = 0;
int number_trees = -1; //weird but has to be -1
int number_logs = 0;
int number_planks = 0;
int number_preboxes = 0;
int number_boxes = 0;
bool isWorking[4] = { false, false, false, false };

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);


string GetTime() {
	auto nowTime = chrono::system_clock::now();
	time_t sleepTime =
		chrono::system_clock::to_time_t(nowTime);
	return ctime(&sleepTime);
}

mutex itemsLock;

int amound_take[] = { 10, 1, 1, 1 };
//int amound_give[] = { 5, 5, 6, 1 };
int amound_give[] = { 2, 3, 4, 1 };

void Work(int id) {

	lock_guard<mutex> lock(itemsLock);

	int sleep_time = 10;

	if (id == 0) {
		SetConsoleTextAttribute(hConsole, 12);
		if (number_trees - amound_take[id] >= 0)
		{
			isWorking[id] = true;
			this_thread::sleep_for(chrono::milliseconds(sleep_time));
			cout << "AxeMan takes " << amound_take[id] << " tree to produce " << amound_give[id] << " logs on " << GetTime();
			number_trees -= amound_take[id];
			number_logs += amound_give[id];
			cout << "\tAmound of tree is: " << number_trees << endl;
			cout << "\tAmound of logs is: " << number_logs << endl << endl;
			
		}
		//else cout << "...AxeMan waits..." << endl;
		SetConsoleTextAttribute(hConsole, 15);
	}

	else if ( id == 1) {
		SetConsoleTextAttribute(hConsole, 14);
		if (number_logs - amound_take[id] >= 0)
		{
			isWorking[id] = true;
			this_thread::sleep_for(chrono::milliseconds(sleep_time));
			cout << "SawMan takes " << amound_take[id] << " log to produce " << amound_give[id] << " planks on " << GetTime();
			number_logs -= amound_take[id];
			number_planks += amound_give[id];
			cout << "\tAmound of logs is: " << number_logs << endl;
			cout << "\tAmound of planks is: " << number_planks << endl << endl;
		}
		//else cout << "...SawMan waits..." << endl;
		SetConsoleTextAttribute(hConsole, 15);
	}

	else if (id == 2) {
			SetConsoleTextAttribute(hConsole, 11);
		if (number_planks - amound_take[id] >= 0)
		{
			isWorking[id] = true;
			this_thread::sleep_for(chrono::milliseconds(sleep_time));
			cout << "HammerMan takes " << amound_take[id] << " planks to produce " << amound_give[id] << " prebox on " << GetTime();
			number_planks -= amound_take[id];
			number_preboxes += amound_give[id];
			cout << "\tAmound of planks is: " << number_planks << endl;
			cout << "\tAmound of preboxes is: " << number_preboxes << endl << endl;
		}
		//else cout << "...HammerMan waits..." << endl;
		SetConsoleTextAttribute(hConsole, 15);
	}

	else if (id == 3) {
		SetConsoleTextAttribute(hConsole, 10);
		if (number_preboxes - amound_take[id] >= 0)
		{
			isWorking[id] = true;
			this_thread::sleep_for(chrono::milliseconds(sleep_time));
			cout << "BrushMan takes 1 prebox to produce 1 box on " << GetTime();
			number_preboxes -= amound_take[id];
			number_boxes += amound_give[id];
			cout << "\tAmound of preboxes is: " << number_preboxes << endl;
			cout << "\tAmound of boxes is: " << number_boxes << endl << endl;
		}

		//else cout << "...BrushMan waits..." << endl;
		SetConsoleTextAttribute(hConsole, 15);
	}

	else cout << "Cannot find this worker!" << endl;
}

int main(int argc, char **argv)
{
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;

	/// Initializing Allegro
	if (!al_init()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize allegro!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	al_install_keyboard();

	timer = al_create_timer(1.0 / FPS);
	if (!timer) {
		al_show_native_message_box(display, "Error", "Error", "Failed to creat the timer!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}
	al_init_font_addon();
	if (!al_init_font_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize font_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}
	al_init_ttf_addon();
	if (!al_init_ttf_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize ttf_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}
	al_init_image_addon();
	if (!al_init_image_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize image_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}
	if (!al_install_audio()) {
		fprintf(stderr, "failed to initialize audio!\n");
		return -1;
	}
	if (!al_init_acodec_addon()) {
		fprintf(stderr, "failed to initialize audio codecs!\n");
		return -1;
	}
	if (!al_reserve_samples(5)) {
		fprintf(stderr, "failed to reserve samples!\n");
		return -1;
	}
	al_init_primitives_addon();
	if (!al_init_primitives_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize primitives_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}
	display = al_create_display(SCREEN_W, SCREEN_H);
	al_set_window_title(display, "How Box Was Made");
	al_set_new_display_flags(ALLEGRO_WINDOWED);
	if (!display) {
		al_show_native_message_box(display, "Error", "Error", "Failed to create the display!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	/// Colors
	ALLEGRO_COLOR color_white = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR color_black = al_map_rgb(1, 1, 1);
	ALLEGRO_COLOR color_gray = al_map_rgb(60, 60, 60);

	/// Fonts
	ALLEGRO_FONT *font8 = al_create_builtin_font();
	ALLEGRO_FONT *couree = al_load_ttf_font("arial.ttf", 24, 0);
	ALLEGRO_FONT *couree_8 = al_load_ttf_font("arial.ttf", 8, 1);
	ALLEGRO_FONT *couree_12 = al_load_ttf_font("arial.ttf", 12, 2);
	ALLEGRO_FONT *couree_16 = al_load_ttf_font("arial.ttf", font_size, 0);
	ALLEGRO_FONT *couree_24 = al_load_ttf_font("arial.ttf", 24, 4);
	ALLEGRO_FONT *couree_32 = al_load_ttf_font("arial.ttf", 32, 4);

	/// Images
	ALLEGRO_BITMAP *background = NULL;
	ALLEGRO_BITMAP *axe_1 = NULL;
	ALLEGRO_BITMAP *axe_2 = NULL; 
	ALLEGRO_BITMAP *axe_3 = NULL;
	ALLEGRO_BITMAP *axe_4 = NULL;
	ALLEGRO_BITMAP *axe_5 = NULL;
	ALLEGRO_BITMAP *saw_1 = NULL;
	ALLEGRO_BITMAP *saw_2 = NULL;
	ALLEGRO_BITMAP *saw_3 = NULL;
	ALLEGRO_BITMAP *saw_4 = NULL;
	ALLEGRO_BITMAP *saw_5 = NULL;
	ALLEGRO_BITMAP *hammer_1 = NULL;
	ALLEGRO_BITMAP *hammer_2 = NULL;
	ALLEGRO_BITMAP *hammer_3 = NULL;
	ALLEGRO_BITMAP *hammer_4 = NULL;
	ALLEGRO_BITMAP *hammer_5 = NULL;
	ALLEGRO_BITMAP *brush_1 = NULL;
	ALLEGRO_BITMAP *brush_2 = NULL;
	ALLEGRO_BITMAP *brush_3 = NULL;
	ALLEGRO_BITMAP *brush_4 = NULL;
	ALLEGRO_BITMAP *brush_5 = NULL;
	ALLEGRO_BITMAP *tree = NULL;
	ALLEGRO_BITMAP *logs = NULL;
	ALLEGRO_BITMAP *planks = NULL;
	ALLEGRO_BITMAP *prebox = NULL;
	ALLEGRO_BITMAP *box = NULL;


	/// Controllers check
	ALLEGRO_KEYBOARD_STATE keyboard;

	/// Load Files
	background = al_load_bitmap("background.png");
	axe_1 = al_load_bitmap("axe1.png");
	axe_2 = al_load_bitmap("axe2.png");
	axe_3 = al_load_bitmap("axe3.png");
	axe_4 = al_load_bitmap("axe4.png");
	axe_5 = al_load_bitmap("axe5.png");
	saw_1 = al_load_bitmap("saw1.png");
	saw_2 = al_load_bitmap("saw2.png");
	saw_3 = al_load_bitmap("saw3.png");
	saw_4 = al_load_bitmap("saw4.png");
	saw_5 = al_load_bitmap("saw5.png");
	hammer_1 = al_load_bitmap("hammer1.png");
	hammer_2 = al_load_bitmap("hammer2.png");
	hammer_3 = al_load_bitmap("hammer3.png");
	hammer_4 = al_load_bitmap("hammer4.png");
	hammer_5 = al_load_bitmap("hammer5.png");
	brush_1 = al_load_bitmap("brush1.png");
	brush_2 = al_load_bitmap("brush2.png");
	brush_3 = al_load_bitmap("brush3.png");
	brush_4 = al_load_bitmap("brush4.png");
	brush_5 = al_load_bitmap("brush5.png");
	tree = al_load_bitmap("tree.png");
	logs = al_load_bitmap("logs.png");
	planks = al_load_bitmap("planks.png");
	prebox = al_load_bitmap("prebox.png");
	box = al_load_bitmap("box.png");

	event_queue = al_create_event_queue();
	if (!event_queue) {
		al_show_native_message_box(display, "Error", "Error", "Failed to create event_queue!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_start_timer(timer);

	float app_time = al_get_time();

	// START THREADS

	thread threads[4];

	// END THREADS

	while (true)
	{
		for (int i = 0; i < 4; i++) isWorking[i] = false;
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		/// Windows Screen Exit
		if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) break;
		
		/// Frame Counting
		if (ev.type == ALLEGRO_EVENT_TIMER)
		{

		}

		/// Keyboard interaction
		if (ev.type == ALLEGRO_EVENT_TIMER) {
			if (al_key_down(&keyboard, ALLEGRO_KEY_SPACE)) {
				number_trees += 100;
				cout << endl << "@ Added 100 Tree | Amoud of Trees:  " << number_trees << endl << endl;
			}
			if (al_key_down(&keyboard, ALLEGRO_KEY_Q)) {
				number_trees += 1;
				cout << endl << "@ Added 1 Tree | Amoud of Trees:  " << number_trees << endl << endl;
			}
			if (al_key_down(&keyboard, ALLEGRO_KEY_W)) {
				number_logs += 1;
				cout << endl << "@ Added 1 Log | Amoud of Logs:  " << number_logs << endl << endl;
			}
			if (al_key_down(&keyboard, ALLEGRO_KEY_E)) {
				number_planks += 1;
				cout << endl << "@ Added 1 Plank | Amoud of Planks:  " << number_planks << endl << endl;
			}
			if (al_key_down(&keyboard, ALLEGRO_KEY_R)) {
				number_preboxes += 1;
				cout << endl << "@ Added 1 Prebox | Amoud of Preboxes:  " << number_preboxes << endl << endl;
			}

			if (al_key_down(&keyboard, ALLEGRO_KEY_X)) {
				number_trees = 0;
				number_logs = 0;
				number_planks = 0;
				number_preboxes = 0;
				number_boxes = 0;
				cout << endl << "@ Deleted ALL !!!!!!!" << endl << endl;
			}

			}

		/// Reading Keyboard and Mouse
		al_get_keyboard_state(&keyboard);

		al_clear_to_color(color_white);
		app_time = al_get_time();

		// THREADS
		/*
		if ((number_trees - 1) >= 0) { threads[0] = thread(Work_Axe, 0, 1, 3, 0); }
		else cout << "Not Enough Trees to take" << endl;

		if ((number_logs - 1) >= 0) { threads[1] = thread(Work_Saw, 1, 1, 5, 0); }
		else cout << "Not Enough Logs to take" << endl;

		if ((number_planks - 6) >= 0) { threads[2] = thread(Work_Hammer, 2, 6, 1, 0); }
		else cout << "Not Enough Planks to take" << endl;

		if ((number_preboxes - 1) >= 0) { threads[3] = thread(Work_Brush, 3, 1, 1, 0); }
		else cout << "Not Enough Preboxes to take" << endl;
		*/

		for (int i = 0; i < 4; i++) threads[i] = thread(Work, i);
		for (int i = 0; i < 4; i++) threads[i].join();
		// END THREADS


		al_draw_textf(couree_16, color_black, 0, 0 , 0, "Hello World!");
		al_draw_textf(couree_16, color_black, 0, font_size * 1, 0, "Frame: %i", frame_count);
		al_draw_textf(couree_16, color_black, 0, font_size * 2 , 0, "Time Passed: %i seconds", frame_count/60);
		al_draw_textf(couree_16, color_black, 0, font_size * 3, 0, "IsWorking %d %d %d %d", isWorking[0], isWorking[1], isWorking[2], isWorking[3]);
		al_draw_textf(couree_32, color_black, 650, font_size * 4 + 75, 0, "%i / 10", number_trees);
		al_draw_textf(couree_32, color_black, 650, font_size * 12 + 75, 0, "%i", number_logs);
		al_draw_textf(couree_32, color_black, 650, font_size * 20 + 75, 0, "%i", number_planks);
		al_draw_textf(couree_32, color_black, 650, font_size * 28 + 75, 0, "%i ", number_preboxes);
		al_draw_textf(couree_32, color_black, 650, font_size * 36 + 75, 0, "%i", number_boxes);

		al_draw_textf(couree_12, color_black, 350, 760, 0, "    SPACE            Q             W               E               R             X");
		al_draw_textf(couree_12, color_gray, 350, 780, 0, "+100 Tree    +1 Tree    +1 Log     +1 Plank    +1 Box     Reset");


		al_draw_bitmap(tree, 500, font_size * 4 + 30, 0);
		al_draw_bitmap(logs, 500, font_size * 12 + 30, 0);
		al_draw_bitmap(planks, 500, font_size * 20 + 30, 0);
		al_draw_bitmap(prebox, 500, font_size * 28 + 30, 0);
		al_draw_bitmap(box, 500, font_size * 36 + 30, 0);

		int animation_slow = 1; //4 dla 0 sleepa

		if (isWorking[0] == true)
		{
			if ((frame_count / animation_slow) % 10 == 0) al_draw_bitmap(axe_1, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 1) al_draw_bitmap(axe_2, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 2) al_draw_bitmap(axe_3, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 3) al_draw_bitmap(axe_4, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 4) al_draw_bitmap(axe_5, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 5) al_draw_bitmap(axe_5, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 6) al_draw_bitmap(axe_4, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 7) al_draw_bitmap(axe_3, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 8) al_draw_bitmap(axe_2, 200, font_size * 12, 0);
			if ((frame_count / animation_slow) % 10 == 9) al_draw_bitmap(axe_1, 200, font_size * 12, 0);
		} else al_draw_bitmap(axe_1, 200, font_size * 12, 0);

		if (isWorking[1] == true)
		{
			if ((frame_count / animation_slow) % 10 == 0) al_draw_bitmap(saw_1, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 1) al_draw_bitmap(saw_2, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 2) al_draw_bitmap(saw_3, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 3) al_draw_bitmap(saw_4, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 4) al_draw_bitmap(saw_5, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 5) al_draw_bitmap(saw_5, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 6) al_draw_bitmap(saw_4, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 7) al_draw_bitmap(saw_3, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 8) al_draw_bitmap(saw_2, 200, font_size * 20, 0);
			if ((frame_count / animation_slow) % 10 == 9) al_draw_bitmap(saw_1, 200, font_size * 20, 0);
		} else al_draw_bitmap(saw_1, 200, font_size * 20, 0);

		if (isWorking[2] == true)
		{
			if ((frame_count / animation_slow) % 10 == 0) al_draw_bitmap(hammer_1, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 1) al_draw_bitmap(hammer_2, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 2) al_draw_bitmap(hammer_3, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 3) al_draw_bitmap(hammer_4, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 4) al_draw_bitmap(hammer_5, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 5) al_draw_bitmap(hammer_5, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 6) al_draw_bitmap(hammer_4, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 7) al_draw_bitmap(hammer_3, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 8) al_draw_bitmap(hammer_2, 200, font_size * 28, 0);
			if ((frame_count / animation_slow) % 10 == 9) al_draw_bitmap(hammer_1, 200, font_size * 28, 0);
		} else al_draw_bitmap(hammer_1, 200, font_size * 28, 0);

		if (isWorking[3] == true)
		{
			if ((frame_count / animation_slow) % 10 == 0) al_draw_bitmap(brush_1, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 1) al_draw_bitmap(brush_2, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 2) al_draw_bitmap(brush_3, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 3) al_draw_bitmap(brush_4, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 4) al_draw_bitmap(brush_5, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 5) al_draw_bitmap(brush_5, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 6) al_draw_bitmap(brush_4, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 7) al_draw_bitmap(brush_3, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 8) al_draw_bitmap(brush_2, 200, font_size * 36, 0);
			if ((frame_count / animation_slow) % 10 == 9) al_draw_bitmap(brush_1, 200, font_size * 36, 0);
		} else al_draw_bitmap(brush_1, 200, font_size * 36, 0);

		if (al_key_down(&keyboard, ALLEGRO_KEY_ESCAPE))
		{
			al_uninstall_keyboard();
			break;
		}

		al_flip_display();
		frame_count++;
	}

	al_destroy_bitmap(background );
	al_destroy_bitmap(axe_1 );
	al_destroy_bitmap(axe_2 );
	al_destroy_bitmap(axe_3 );
	al_destroy_bitmap(axe_4 );
	al_destroy_bitmap(axe_5);
	al_destroy_bitmap(saw_1 );
	al_destroy_bitmap(saw_2 );
	al_destroy_bitmap(saw_3 );
	al_destroy_bitmap(saw_4);
	al_destroy_bitmap(saw_5 );
	al_destroy_bitmap(hammer_1 );
	al_destroy_bitmap(hammer_2 );
	al_destroy_bitmap(hammer_3 );
	al_destroy_bitmap(hammer_4 );
	al_destroy_bitmap(hammer_5);
	al_destroy_bitmap(brush_1 );
	al_destroy_bitmap(brush_2 );
	al_destroy_bitmap(brush_3 );
	al_destroy_bitmap(brush_4 );
	al_destroy_bitmap(brush_5);
	al_destroy_bitmap(tree );
	al_destroy_bitmap(logs );
	al_destroy_bitmap(planks );
	al_destroy_bitmap(prebox );
	al_destroy_bitmap(box );
	al_destroy_font(font8);
	al_destroy_font(couree);
	al_destroy_font(couree_8);
	al_destroy_font(couree_12);
	al_destroy_font(couree_16);
	al_destroy_font(couree_24);
	al_destroy_font(couree_32);
	al_destroy_timer(timer);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);

	//system("pause");
	return 0;
}